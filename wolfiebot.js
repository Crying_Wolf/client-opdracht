const Discord = require("discord.js");
const bot = new Discord.Client();
const config = require('./config.json');
const bottoken = config.ownToken;
const fs = require('fs');
const stdin = process.openStdin();
const pug = require('pug');

let values = require('./values.json');
let prefix = config.prefix;
var file = "database.db"
var sqlite3 = require("sqlite3").verbose();
var repliedWhileNotOnline = [];
var toIgnoreServers = ["OSBuddy"];

var viewsfolder = "/frontend/views";
var express = require('express')
  , app = express()
  , http = require('http')
  , sio = require('socket.io')
  , server = http.createServer(app)
  , io = sio.listen(server);

  server.listen(8080);
  app.set('view engine', 'pug');
  app.use(express.static('frontend/public'));

  app.get('/', function (req, res) {
    res.render(__dirname + viewsfolder + '/homepage.pug');
  });

  app.get('/settings', function (req, res){
    res.render(__dirname + viewsfolder + '/settings.pug');
  });

  app.get('/messagedata', function (req, res){
    res.render(__dirname + viewsfolder + '/messagedata.pug');
  });

  app.get('/test', function(req, res){
    res.render(__dirname + viewsfolder + '/template.pug', { name: "Wolfy" })
  });

io.sockets.on('connection', function (socket) {
  socket.on('getgeneraldata', function(){
    if(bot.user){
      socket.emit("generaldata", bot.user.tag, process.uptime(), bot.guilds.size, bot.channels.size, bot.users.size, bot.user.premium);
    }
  });

  socket.on('getvalues', function(){
    console.log("Sending values of bot to client.");
    socket.emit('botvalues', bot.values);
  });

  socket.on('setvalue', function(valueName, value){
    let alterValuesCommand = require("./commands/altervalues");
    alterValuesCommand.run(bot, false, [valueName, value]);
  });

  socket.on('getguilds', function(){
    console.log("sending all guilds to client.");
    socket.emit('guildlist', bot.guilds.array());
  });

  socket.on('getchannelofguild', function(guildid){
    console.log("getting ID from channel");
    if(bot.guilds.has(guildid)){
      console.log("guild ID found!");
      var guild = bot.guilds.get(guildid);
      socket.emit('channellist', guild.channels.array());
    } else {
      console.log("no guild ID found.");
    }
  });

  socket.on('getmessagesofchannel', function(channelID, amount, maxDate = 0, shouldCompareDate = false){
    let messageDataBrowser = require("./scripts/messageDataForBrowserV2.js");
    if(amount < 10 || amount > 5000 || amount == null){
      amount = 100;
    }
    messageDataBrowser.run(bot, socket, bot.channels.get(channelID), amount, function(socket, messagearray){
      socket.emit('messagelist', messagearray);
    },
    maxDate,
    shouldCompareDate
    );
  });

  socket.on('test', function(variable){
    console.log(`contents of variable: ${variable}`);
    socket.emit('test', variable);
  });
});

function processDataMessages(socket, array){
  var newArray = [];
  for(var i = 0; i < array.length; i++){
    var message = {
      content : array[i].content,
      author : array[i].author,
      date : array[i].createdAt
    };

    setTimeout(function(message){
      newArray.push(message);
      console.log(message);
    }, 10);
    if(i == array.length -1){
      socket.emit("messagelist", newArray);
    }
  }
}

bot.db = new sqlite3.Database(file);
bot.shortcuts = new Map();
bot.values = values;
bot.nyxreplies = {};

var messagecounter = 0;

function replaceWords(words, msg, callback) {
    if (!bot.values.replacewords) return;
    if (words.size < 1) return;
    var tempMessage = msg.content;
    var counter = 0;
    var replaced = false;
    for (let [k, v] of words) {
        if (tempMessage.split(k).length - 1 > 0) {
            var re = new RegExp(k, "g");
            tempMessage = tempMessage.replace(re, v);
            replaced = true;
        }
        counter++;
        if (counter >= words.size && replaced) {
            callback(msg, tempMessage);
        }
    }
}

function afkreply(msg, status) {
    if (msg.author == bot.user || repliedWhileNotOnline.indexOf(msg.author.id) > -1 || bot.values.autoreply) return;

    switch (status) {
        case "idle":
            msg.reply(`Autoreply: it seems I'm idle atm. I'll respond to you as soon as I can.`).then(repliedToAndAddToArray(msg.author)).catch(console.error);
            break;
        case "offline":
            msg.reply(`Autoreply: it seems I'm offline or invisible atm. I'll respond to you as soon as I can.\nMy bot seems to be running though, otherwise you would not see this message :P`).then(repliedToAndAddToArray(msg.author)).catch(console.error);
            break;
        case "dnd":
            msg.reply(`Autoreply: it seems I'm in do not disturb mode. I'll respond to you as soon as I can.`).then(repliedToAndAddToArray(msg.author)).catch(console.error);
            break;
    }
}

function repliedToAndAddToArray(user) {
    console.log(`replied to ${user.username}.`)
    repliedWhileNotOnline.push(user.id);
}

function addToWatchList(id) {
    console.log(`Added ID ${id} to list.`);
    watchList.push(id);
}

function saveMessageToDatabase(msg /*messageID, playerID, author, guild, channel, content, timestamp*/ ) {

    if (!bot.values.logtodb) return;
    var messageID = msg.id;
    var playerID = msg.author.id;
    var author = msg.author.username;
    var guild = msg.guild.name;
    var channel = msg.channel.name;
    var content = msg.content;
    var timestamp = msg.createdAt;

    var stmt = bot.db.prepare("INSERT INTO messages VALUES (?,?,?,?,?,?,?)");
    stmt.run(messageID, playerID, author, guild, channel, content, timestamp);
    messagecounter++;
    console.log("Succesfully added row to database! | " + messagecounter + " messages since startup...");
}

function logMessage(msg) {
    if (!bot.values.logmsg) return;
    try {
        if (msg.channel.type == "text" && toIgnoreServers.indexOf(msg.guild.name) < 0) {
            if (msg.isMentioned(bot.user.id)) {
                console.log(`[MENTION] ${msg.author.username} (${msg.author.id}) on ${msg.guild.name}/${msg.channel.name}:\n${msg.content}\n`);
                return;
            }
            console.log(`::(${msg.author.username}) in ${msg.channel.name} on ${msg.guild}:\n ${msg.content}\n`);
            saveMessageToDatabase(msg);
        }
        if (msg.channel.type == "dm") {
            console.log(bot.user.presence.status + " is my status atm.");
            if (bot.user.presence.status !== "online") afkreply(msg, bot.user.presence.status);
            if (msg.isMentioned(bot.user.id)) {
                console.log(`[MENTION] ${msg.author.username} (${msg.author.id}) in DM:\n${msg.content}\n`);
                return;
            }
            console.log(`::(${msg.author.username}) in DM with ${msg.channel.recipient.username}:\n ${msg.content}\n`);
        }
    } catch (e) {
        console.log(e);
    }
}

bot.populateMap = function populateMap() {
    bot.db.each("SELECT shortcut, text FROM shortcuts", function(err, row) {
        bot.shortcuts.set(row.shortcut, row.text);
    });
}

bot.randomInt = function randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function checkForRegex(msg, regex, callback) {
    if (!bot.values.checkregex) return;
    callback(msg, msg.content.match(regex));
}

bot.initReport = function initReport() {
    if (!bot.values.debug) {
        console.log(`Reporting for duty... Ready to spy on ${bot.users.size} users and ${bot.channels.size} channels of ${bot.guilds.size} servers.`);
        var counter = 0;
        for (let [k, v] of bot.users) {
            counter++;
            console.log(`${counter}.\t:${v.username}`);
        }
    } else {
        console.log(`bot is ready.`);
    }
}

bot.on("message", msg => {
    logMessage(msg); //handle messages

    if (msg.channel.type == "text" && bot.values.feed) {
        console.log(`\t:Guild\t\t${msg.channel.guild.name}\n
          :Channel\t${msg.channel.name}\n
          :Name\t\t${msg.author.username}\n
          :Content\t${msg.content}
          \n`);
    }

    if (msg.author !== bot.user) return;
    //check for commands, first checking if prefix is correct
    //followed by splitting the message in words, the first word being checked if it is a valid shortcut
    //if not, in the try/catch block a file with the same name as command from input is located and if it exists,
    //executed using the rest of the message as arguments.
    if (!msg.content.startsWith(prefix)) return;
    const args = msg.content.split(" ");
    const command = args.shift().slice(prefix.length);

    if (bot.shortcuts.has(command)) {
        setTimeout(() => {
            msg.edit(bot.shortcuts.get(command))
        }, 50);
        return;
    }
    try {
        let cmdFile = require("./commands/" + command);
        cmdFile.run(bot, msg, args);
    } catch (e) {
        msg.edit(msg.author + `Error while executing command\n${e}`).then(setTimeout(msg.delete.bind(msg), 10000));
        console.log(e);
    }
});

bot.on("messageDelete", msg => {
    if (!bot.values.messagenotifications) return;
    if (msg.channel.type == "text") {
        //if(msg.channel.guild.name !== "Queens Haven (VQ/SQ/NQ/QD/QG)") return;
        console.log(`message deleted in:\n:Guild\t\t${msg.channel.guild.name}\n:Channel\t${msg.channel.name}\n:Name\t\t${msg.author.username}\n:Content\t${msg.content}`);
    }
    if (msg.channel.type == "dm") {
        console.log(`::(${msg.author.username}) in DM with ${msg.channel.recipient.username} deleted message with content:\n ${msg.content}\n`);
    }
});

bot.on("messageUpdate", (msg, msgNew) => {
    if (!bot.values.messagenotifications) return;
    try {
        if (msg.channel.type == "text" && msg.content != msgNew.content) {
            console.log(`message edited in:\n:Guild\t\t${msg.channel.guild.name}\n:Name\t\t${msg.author.username}\n:Channel\t${msg.channel.name}\n:Old content\t${msg.content}`);
            console.log(`:New content\t${msgNew.content}\n`);
        }
        if (msg.channel.type == "dm") {
            console.log(`::(${msg.author.username}) in DM with ${msg.channel.recipient.username} updated message from:\n ${msg.content}\nto:\n ${msgNew.content}\n`);
        }
    } catch (e) {
        console.log(e);
    }
});

bot.on("presenceUpdate", (oldMember, newMember) => {
    if (!bot.values.presence) return;
    if (oldMember.guild.name !== "Queens Haven (VQ/SQ/NQ/QD/QG)" || oldMember.presence.status == newMember.presence.status) return;
    console.log(`Presence updated:\n:\t${oldMember.user.username} (${oldMember.nickname}) updated presence from ${oldMember.presence.status} > ${newMember.presence.status}`);
});

bot.on('ready', () => {
    bot.populateMap();
    bot.initReport();

});
bot.on('error', console.error);
bot.on('warn', console.warn);
bot.on('disconnect', console.warn);
bot.login(bottoken);

//handle input in the console.
stdin.addListener("data", function(d) {
    var input = d.toString().trim();
    console.log("Data entered: [" + input + "]");
    try {
        console.log(eval(input));
    } catch (e) {
        console.log(e);
    }
});

function Overview(){
  console.log(`Amount of guilds: ${bot.guilds.size}`);
  console.log(`Amount of channels: ${bot.channels.size}`);
  console.log(`Amount of users: ${bot.users.size}`)
}

function PrintGuildsConsole(){
  var guildArray = bot.guilds.array();
  for (var i = 0; i < guildArray.length; i++){
    console.log("Guildname: ")
    console.log(guildArray[i].name);
    var channelArray = guildArray[i].channels.array();
    for (var j = 0; j < channelArray.length; j++){
      console.log(`\t: ${channelArray[j].name}`);
    }
  }
}
