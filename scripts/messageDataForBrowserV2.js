exports.run = function(bot, socket, channel, amount, callback, maximumDate, shouldCompareDate) {
  let numberofmessages = amount;
  let messageSenderMap = new Map();
  let counter = 0;
  let messagecount = amount;
  let messages_array;
  let msgString;
  let maxMessages = 100;
  let messageArray = [];
  let finishedMessagesArray;
  let maxDate = maximumDate;

  function compareDateToMax(date){
    //as this method is currently not being used, always return false.
    return false;
    //console.log(`${maxDate > date} - comparing dates: ${maxDate} > ${date}`);
    //return shouldCompareDate && maxDate > date;
  }

  function fetchMessages(amount, callback, socket){
    if(amount >= maxMessages){
      var messagecount = maxMessages;
      amount = amount - maxMessages
    } else {
      var messagecount = amount; amount = 0;
    }
    channel.fetchMessages({limit: messagecount})
      .then(messages => {
        let msg_array = messages.array();
        let lastMessage = messages.array().slice(-1).pop();
        let messageID = "";
        messageID = lastMessage.id;
        if(messageID !== ""){
          if(amount == 0 || compareDateToMax(lastMessage.createdTimestamp)){
            finishedMessagesArray = msg_array;
            callback(socket, msg_array);
          } else {
            fetchMessagesFollowup(amount, messageID, msg_array, callback, socket);
          }
        }
      });
  }

  function fetchMessagesFollowup(amount, messageID, msg_arr, callback, socket){
    if(amount >= maxMessages){ var messagecount = maxMessages; amount = amount - maxMessages} else {var messagecount = amount; amount = 0;}
    channel.fetchMessages({limit: messagecount, before: messageID})
      .then(messages => {
        let msg_array = msg_arr.concat(messages.array());
        let lastMessage = messages.array().slice(-1).pop();
        let messageID = "";
        messageID = lastMessage.id;
        messageID = messages.array().slice(-1).pop().id;
        if(messageID !== ""){
          if(amount == 0 || compareDateToMax(lastMessage.createdTimestamp)){
            finishedMessagesArray = msg_array;
            callback(socket, msg_array);
          } else {
            fetchMessagesFollowup(amount, messageID, msg_array, callback, socket);
          }
        }
      });
  }

  function convertArrayComponentsToJson(nonjsonmessagesarray, socket, callback){
    jsonmessagesarray = [];
    for (var i = 0; i < nonjsonmessagesarray.length; i++){
      var obj = {
        author: nonjsonmessagesarray[i].author.username,
        content: nonjsonmessagesarray[i].content,
        created: nonjsonmessagesarray[i].createdAt,
        createdTimestamp: nonjsonmessagesarray[i].createdTimestamp
      }
      jsonmessagesarray.push(JSON.stringify(obj));
      if(i + 1 == nonjsonmessagesarray.length){
        console.log("calling callback in convertArrayComponentsToJson");
        callback(socket, jsonmessagesarray);
      }
    }
  }
  function sendBackToBrowser(socket, messages_array){
    setTimeout(function(){
        console.log(`array.length of finishedMessagesArray: ${finishedMessagesArray.length}`);
        convertArrayComponentsToJson(finishedMessagesArray, socket, callback);
      }, 100);
  }

  function init(socket){
    if(shouldCompareDate){
      numberofmessages = 5000;
    }
    fetchMessages(
      numberofmessages,
      function(socket, messages_array){
        sendBackToBrowser(socket, messages_array);
      },
      socket
    );
  }
  init(socket);
};
