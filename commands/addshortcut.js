exports.run = function(bot, msg, args) {
  var stmt = bot.db.prepare("INSERT INTO shortcuts VALUES (?,?)");
  var shortcut = args[0];
  args.shift()
  var text = args.join(" ");
  stmt.run(shortcut, text);
  msg.edit("Succesfully added " + shortcut + " to database! :3").then(setTimeout(msg.delete.bind(msg), 2000));
  bot.populateMap();
};
