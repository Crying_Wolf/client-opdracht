exports.run = function(bot, msg, args) {
  //var name = args.shift();
  var avatarURL = "";
  if(msg.mentions.users.first() == bot.user) {
    avatarURL = bot.user.avatarURL;
  } else {
    var user = msg.mentions.users.first();
    avatarURL = user.avatarURL;
  }
  msg.edit(avatarURL);
};
