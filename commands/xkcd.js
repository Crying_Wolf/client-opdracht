exports.run = function(bot, msg, args) {

    var xkcd = require("xkcd-api");

    xkcd.random(function(error, response) {
      if (error) {
        console.error(error);
      } else {
        var title = response['title'];
        var link = response['img'];
        var num = response['num'];
        setTimeout( () => { msg.edit(`${num} - ${title}\n${link}`) }, 100);
      }
    });

};
