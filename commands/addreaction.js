exports.run = function(bot, msg, args) {
  let messagecount = parseInt(args[0]) ? parseInt(args[0]) : 1;
  msg.channel.fetchMessages({limit: messagecount})
  .then(messages => {
    let msg_array = messages.array();
    for(let message of msg_array){
      message.react('regional_indicator_a');
      message.react('regional_indicator_w');
      message.react('regional_indicator_o');
    }
   });
};
