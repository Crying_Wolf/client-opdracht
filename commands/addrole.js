exports.run = function(bot, msg, args) {
  var memberid = args.shift().slice(2,-1);
  var roleName = args.join(" ");
  console.log(roleName + " - " + memberid);
  var guild = msg.guild;
  var members = guild.members;
  var roles = guild.roles;
  var roleToAdd;
  roles.forEach(function(role, index){
    if(roleName == role.name){
      roleToAdd = role;
    }
  });
  if (roleToAdd != null){
    if(members.has(memberid)){
      var member = members.get(memberid);
      member.addRole(roleToAdd)
      .then(msg.edit(`succeeded in adding role ${roleName} to ${member.user.username}`))
      .catch(console.error);
    }
  }
};
