exports.run = function(bot, msg, args) {

  var guild = msg.guild;
  var members = guild.members;
  var roles = guild.roles;
  var roleToAdd;
  roles.forEach(function(role, index){
    console.log(`available roles: ${role}`);
    if(args[0] == role.name){
      roleToAdd = role;
    }
  });
  if (roleToAdd != null){
    members.forEach(function(member, index){
      console.log(`attempting to add role to member ${member.user.username}`);
      member.addRole(roleToAdd).then(console.log(`succeeded in adding role ${roleToAdd.role} to ${member.user.username}`)).catch(console.error);
    });
  }
};
