exports.run = function(bot, msg, args) {
  
  var length = -1;
    bot.db.get("SELECT count(shortcut) AS count FROM shortcuts", function(err, row) {
      length = row.count;
  });

  function printdata(callback){
    var shortcuts = [];
    bot.db.each("SELECT shortcut, text FROM shortcuts", function(err, row) {
      //console.log(row.shortcut + " - " + row.text);
      shortcuts.push(row.shortcut);
      if(shortcuts.length == length){
        callback(shortcuts);
      }
    });
  }

  printdata(function(arr){
    var str = arr.join("\n");
    msg.edit(str).then(setTimeout(msg.delete.bind(msg), 8000));
  });
};
