exports.run = function(bot, msg, args) {
  const fs = require('fs');
  var variable = args[0];
  var value = args[1];

  alterSettings(variable, value, msg, bot);

  function alterSettings(variable, value, msg, bot){
    var switchActivated = false;
    switch(variable){
      case "debug":
      bot.values.debug = (value == "true") ? true : false;
      switchActivated = true;
      case "watch":
      bot.values.watch = (value == "true") ? true : false;
      switchActivated = true;
      break;
      case "logtodb":
      bot.values.logtodb = (value == "true") ? true : false;
      switchActivated = true;
      break;
      case "replacewords":
      bot.values.replacewords = (value == "true") ? true : false;
      switchActivated = true;
      break;
      case "autoreply":
      bot.values.autoreply = (value == "true") ? true : false;
      switchActivated = true;
      break;
      case "logmsg":
      bot.values.logmsg = (value == "true") ? true : false;
      switchActivated = true;
      case "presence":
      bot.values.presence = (value == "true") ? true : false;
      switchActivated = true;
      break;
      case "checkregex":
      bot.values.checkregex = (value == "true") ? true : false;
      switchActivated = true;
      case "messagenotifications":
      bot.values.messagenotifications = (value == "true") ? true : false;
      switchActivated = true;
      case "wolfiebot":
      bot.values.wolfiebot = (value == "true") ? true : false;
      switchActivated = true;
      case "feed":
      bot.values.feed = (value == "true") ? true : false;
      switchActivated = true;
    }
    if(switchActivated){
      fs.writeFileSync("values.json", JSON.stringify(bot.values));
      if(msg){
        msg.edit(`Variable '${variable}' has been changed to '${value}'.`).then(setTimeout(msg.delete.bind(msg), 3000));
      }
    } else if(msg) {
      msg.edit(`Variable '${variable}' could not be changed to '${value}'.`).then(setTimeout(msg.delete.bind(msg), 3000));
    } else {
      console.log("could not change variable and no message to edit.");
    }
  }

};
