//https://www.toptal.com/javascript/javascript-promises <<<< better implement this :3

exports.run = function(bot, msg, args) {
  let targetUser;
  if(msg.mentions.users.first() == bot.user) {
    targetUser = bot.user;
  } else {
    targetUser = msg.mentions.users.first();
  }
  let days = parseInt(args[1]) ? parseInt(args[1]) : 1;
  let untilTimestamp = getTimestamp(days);



  let numberofmessages = parseInt(args[0]) ? parseInt(args[0]) : 1;

  let messagecount = parseInt(args[0]) ? parseInt(args[0]) : 1;
  let messages_array;

  let maxMessages = 100;

  function getTimestamp(days){
    var currentTimestamp = new Date().getTime();
    return currentTimestamp - (days * 24 * 60 * 60 * 1000);
  }

  function fetchMessages(amount, callback){
    console.log("called fetchMessagesFollowup(), untilTimestamp: " + untilTimestamp);
    if(amount >= maxMessages){ var messagecount = maxMessages; amount = amount - maxMessages} else {var messagecount = amount; amount = 0;}
    msg.channel.fetchMessages({limit: messagecount})
      .then(messages => {
        let msg_array = messages.array();
        let messageID = "";
        let message = messages.array().slice(-1).pop();
        messageID = message.id;
        let messageTimestamp = message.createdTimestamp;
        if(messageID !== ""){
          if(amount == 0 || messageTimestamp <= untilTimestamp){
            console.log("callback called in method fetchMessages(), timestamp: " + messageTimestamp);
            callback(msg_array);

          } else {
            console.log("called method fetchMessagesFollowup(), timestamp: " + messageTimestamp);
            fetchMessagesFollowup(amount, messageID, msg_array, callback);

          }
        }
      });
  }

  function fetchMessagesFollowup(amount, messageID, msg_arr, callback){
    console.log("called fetchMessagesFollowup()");
    if(amount >= maxMessages){ var messagecount = maxMessages; amount = amount - maxMessages} else {var messagecount = amount; amount = 0;}
    msg.channel.fetchMessages({limit: messagecount, before: messageID})
      .then(messages => {
        let msg_array = msg_arr.concat(messages.array());
        let messageID = "";
        let message = messages.array().slice(-1).pop();
        messageID = message.id;
        let messageTimestamp = message.createdTimestamp;
        if(messageID !== ""){
          if(amount == 0 || messageTimestamp <= untilTimestamp){
            console.log("callback called in method fetchMessagesFollowup(), timestamp: " + messageTimestamp);
            callback(msg_array);

          } else {
            console.log("called new method fetchMessagesFollowup(), timestamp: " + messageTimestamp);
            fetchMessagesFollowup(amount, messageID, msg_array, callback);

          }
        }
      });
  }

  function processData(messages_array){
    var pruned = false;
    console.log("processData has been called~, array length is " + messages_array.length);
    console.log(`first: ${messages_array[0].createdTimestamp}\nlast: ${messages_array[messages_array.length - 1].createdTimestamp}`);
    if(messages_array[messages_array.length - 1].createdTimestamp <= untilTimestamp){
      while(messages_array[messages_array.length - 1].createdTimestamp <= untilTimestamp){
        messages_array.pop();
        if(!messages_array[messages_array.length - 1].createdTimestamp <= untilTimestamp){
          pruned = true;
        }
      }
    } else {
      pruned = true;
    }
    if(pruned){
      console.log(`After pruning, ${messages_array.length} messages in array left.`);
    }

  }


  fetchMessages(numberofmessages, function(msg_array){messages_array = msg_array; console.log(`done, ${messages_array.length} items in global array.`);  processData(messages_array);});
};
