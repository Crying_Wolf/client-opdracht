exports.run = function(bot, msg, args) {
  var memberid = args.shift().slice(2,-1);
  var roleName = args.join(" ");
  console.log(roleName + " - " + memberid);
  var guild = msg.guild;
  var members = guild.members;
  var roles = guild.roles;
  var roleToRemove;
  roles.forEach(function(role, index){
    if(roleName == role.name){
      roleToRemove = role;
    }
  });
  if (roleToRemove != null){
    if(members.has(memberid)){
      var member = members.get(memberid);
      member.removeRole(roleToRemove)
      .then(msg.edit(`succeeded in removing role ${roleName} from ${member.user.username}`))
      .catch(console.error);
    }
  }
};
