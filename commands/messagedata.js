//https://www.toptal.com/javascript/javascript-promises <<<< better implement this :3

exports.run = function(bot, msg, args) {
  let numberofmessages = parseInt(args[0]) ? parseInt(args[0]) : 1;
  let messageSenderMap = new Map();
  let counter = 0;
  let messagecount = parseInt(args[0]) ? parseInt(args[0]) : 1;
  let messages_array;
  let msgString;
  let maxMessages = 100;

  function fetchMessages(amount, callback){
    //console.log("called fetchMessagesFollowup()");
    if(amount >= maxMessages){ var messagecount = maxMessages; amount = amount - maxMessages} else {var messagecount = amount; amount = 0;}
    msg.channel.fetchMessages({limit: messagecount})
      .then(messages => {
        let msg_array = messages.array();
        //console.log(messages.array().slice(-1).pop().id);
        //console.log(messages.array().slice(-1));
        //console.log(messages.array().slice(-1).pop());
        let messageID = "";
        messageID = messages.array().slice(-1).pop().id;
        if(messageID !== ""){
          if(amount == 0){
            callback(msg_array);
            //console.log("callback called in method fetchMessages()");
          } else {
            fetchMessagesFollowup(amount, messageID, msg_array, callback);
            //console.log("called method fetchMessagesFollowup()");
          }
        }
      });
  }

  function fetchMessagesFollowup(amount, messageID, msg_arr, callback){
    //console.log("called fetchMessagesFollowup()");
    if(amount >= maxMessages){ var messagecount = maxMessages; amount = amount - maxMessages} else {var messagecount = amount; amount = 0;}
    msg.channel.fetchMessages({limit: messagecount, before: messageID})
      .then(messages => {
        let msg_array = msg_arr.concat(messages.array());
        let messageID = "";
        messageID = messages.array().slice(-1).pop().id;
        if(messageID !== ""){
          if(amount == 0){
            callback(msg_array);
            //console.log("callback called in method fetchMessagesFollowup()");
          } else {
            fetchMessagesFollowup(amount, messageID, msg_array, callback);
            //console.log("called new method fetchMessagesFollowup()");
          }
        }
      });
  }

  function processData(msg_array){
    messagecount = msg_array.length;
    msgString = `\`\`\`markdown\nStatistics last ${messagecount} messages in this channel:\nformat:\nprct\t-\tamount\t-\tname\n`;
    //console.log(`there are ${messagecount} messages in the array. (from processdata)`);
    msg_array.forEach(function(message, index){
    counter++;
      //console.log(message.content);
      if(messageSenderMap.has(message.author.username)){
        messageSenderMap.set(message.author.username, messageSenderMap.get(message.author.username) + 1);
      } else {
        messageSenderMap.set(message.author.username, 1);
      }

      if(messagecount == counter){
        //console.log("done!");
        var totalusers = messageSenderMap.size;
        let counter2 = 0;
        for(let[k,v] of messageSenderMap){
          counter2++;
          //var percentage = 10*(Math.round(((100/messagecount)*v)*10));
          var percentage = Math.round(100/messagecount*v);
          //console.log(`${k} has send ${v} messages, making up ${percentage} the messages.\n`);
          //msgString += `${k} send ${v} messages, making up about ${percentage}%.\n`;
          msgString += `${percentage}%\t-\t${v}\t-\t${k}\n`;
            if(counter2 == messageSenderMap.size){
              msgString += "```";
              console.log(msgString);
              msg.edit(msgString);
              setTimeout( () => { msg.edit(msgString) }, 50);
            }
        }
      }

    });
  }

  function mapToArrayAndSort(callback){
    //var map = swapM(new Map([["cry", 20],["zylonoob", 90],["stronk", 6],["kek", 9],["cry2", 22],["zylonoob2", 92],["stronk2", 8],["kek2", 19]]));
    //console.log(map);
    sort(swapM(messageSenderMap), callback);
  }

  function swapM(map){
    console.log("swapping map");
    var ret = new Map();
    for(var [k,v] of map){
      //ret[json[key]] = key;
      ret.set(v,k);
    }
    return ret;
  }

  function sort(map, callback){
  var arr = [];
  var map2 = new Map();
    for(var[k,v] of map){
     arr.push(k);
    }
    arr = arr.sort(function compareNumbers(a, b) {
    return a - b;
    }).reverse();
    for(i = 0; i < arr.length; i++){
      map2.set(arr[i], map.get(arr[i]));
      if(i == arr.length - 1){callback(swapM(map2));}
    }
    //return "nothing";
  }
/*
mapToArrayAndSort(function (map){
  messageSenderMap = map;
  console.log("callback with sorted messagesendermap called!");
});
*/
  fetchMessages(numberofmessages, function(msg_array){messages_array = msg_array; console.log(`done, ${messages_array.length} items in global array.`);  processData(messages_array);});
};
