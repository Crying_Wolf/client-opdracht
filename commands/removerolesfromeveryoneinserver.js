exports.run = function(bot, msg, args) {

  var guild = msg.guild;
  var members = guild.members;
  var roles = guild.roles;
  var roleToRemove;
  roles.forEach(function(role, index){
    console.log(`available roles: ${role}`);
    if(args[0] == role.name){
      roleToRemove = role;
    }
  });
  if (roleToRemove != null){
    members.forEach(function(member, index){
      console.log(`attempting to remove role from member. ${member.user.username}`);
      member.removeRole(roleToRemove).then(console.log(`succeeded in removing role ${roleToRemove.role} from ${member.user.username}`)).catch(console.error);
    });
  }
};
