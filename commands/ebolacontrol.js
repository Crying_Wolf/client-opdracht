exports.run = function(bot, msg, args) {
  let ban_id;
  let ban_uname = args[0];
  let days = args[1];
  var ban = false;
  var arr = msg.guild.members.array();
  for(var i = 0; i < arr.length; i++){
    if(arr[i].user.username === ban_uname){
      ban_id = arr[i].user.id;
      ban = true;
      msg.guild.ban(ban_id, days)
        .then( () => console.log(`Banned ${ban_id} and removed ${days} days of messages`))
        .catch(console.error);
      break;
    }
  }
  if(ban){
    setTimeout( () => { msg.edit("Ebolacontrol on " + ban_uname + " activated! bye bye~") }, 50);
  } else {
    setTimeout( () => { msg.edit("Ebolacontrol on " + ban_uname + " could not be activated. Username not found :c") }, 50);
  }
};
