exports.run = function(bot, msg, args) {
  var messagestring = args.join(" ");
  var endstring = "";
  //str = str.replace(/\s+/g, '+');
  messagestring  = messagestring.replace(/[^a-zA-Z\s]/g, "");
  for (var i = 0, len = messagestring.length; i < len; i++) {
    var char = messagestring.charAt(i);
    if(char == " "){
      endstring += "    ";
    } else {
      endstring += `:regional_indicator_${char}: `;
    }
  }
  setTimeout( () => {
    msg.channel.sendMessage(endstring);
    msg.delete();
  }, 500);
};
