exports.run = function(bot, msg, args) {
  let lang = args.shift();
  let code = args.join(" ");
  msg.editCode(lang, code);
};
