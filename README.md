# README #

Hello, this is my personal pet project, now with some frontend stuff for the client technology project.

### How do I get set up? ###

Try to run 'node wolfiebot.js' and install any missing dependencies with 'npm install' that prevent the bot from running.
All dependencies should be in the package-lock.json file.
Oh, and don't forget to put your discord token in the config.json file.

It should be possible to run javascript commands by simply typing in the terminal.