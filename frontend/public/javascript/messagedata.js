var MessageData = (function(){
  //private variables
  var _guilds;
  var _channels;
  var _selectedGuildID;
  var _selectedChannelID;
  var _messages;
  var _initialized = false;

  //private methods
  var _getGuilds = function(){
    socket.emit('getguilds');
  }

  var _getChannels = function(guildID){
    socket.emit('getchannelofguild', guildID);
  }

  var _getMessages = function(channelID, amount, maxDate = 0, shouldCompareDate = false){
    console.log("emitting getmessages, " + channelID + " amount " + amount);
    socket.emit('getmessagesofchannel', channelID, amount, maxDate, shouldCompareDate);
  }

  var _setGuilds = function(guildArray){
    _guilds = guildArray;
    UIElements.generateList(guildArray);
  }

  var _setChannels = function(channelArray){
    _channels = channelArray;
    UIElements.generateChannelList(channelArray);
  }

  var _setMessages = function(messageArray){
    jsonParsedArray = [];
    for (var i = 0; i < messageArray.length; i++){
      jsonParsedArray.push(JSON.parse(messageArray[i]));
    }
    _messages = jsonParsedArray;
  }

  //listeners socket calls from servers
  socket.on('guildlist', function(guilds){
    _setGuilds(guilds);
    _initialized = true;
  });

  socket.on('channellist', function(channels){
    _setChannels(channels);
  });

  socket.on('messagelist', function(messages){
    _setMessages(messages);
    console.log("received messages.");
    UIElements.enableGraphButton();
  });

  socket.on('test', function(variable){
    console.log("received (test), " + variable);
  });

  //public methods
  var init = function(){
    console.log("calling init function..");
    _getGuilds();
  }

  var printGuildsConsole = function(){
    console.log(_guilds);
    if(!_guilds) return;
    var guildArray = _guilds;
    for (var i = 0; i < guildArray.length; i++){
      console.log("Guildname: ")
      console.log(guildArray[i].name);
    }
    return _guilds;
  }

  var setSelectedGuildID = function(guildID){
    _selectedGuildID = guildID;
    console.log("set selected guildID to " + guildID);
    _getChannels(guildID);
  }

  var setSelectedChannelID = function(channelID){
    _selectedChannelID = channelID;
    console.log("set selected channelID to " + channelID);
  }

  var getGuilds = function(){
    if(!_guilds){
      return;
    }
    return _guilds;
  }

  var getMessages = function(){
    console.log("getting messages..");
    var date = document.getElementById('datepicker').value;
    var maxDate = new Date(date);
    if(UIElements.checkIfInputIsValid() && _selectedChannelID != null){
      console.log("input validated. getting messages with amount and maxdate attached.");
      //getting variables using document.getElementById because the method
      //in UIElements doesn't seem to be working properly.
      var date = document.getElementById('datepicker').value;
      var maxDate = new Date(date);
      maxDate = Date.parse(maxDate);
      var amount = parseInt(document.getElementById('amount').value);
      _getMessages(_selectedChannelID, amount, maxDate, false);
    }
  }

  var printMessages = function(){
    console.log(_messages);
  }

  var hasInitialized = function(){
    return _initialized;
  }

  var getMessagesArray = function(){
    return _messages;
  }


  return {
      init: init,
      printGuildsConsole : printGuildsConsole,
      guilds : getGuilds,
      setSelectedGuildID : setSelectedGuildID,
      setSelectedChannelID : setSelectedChannelID,
      getMessages : getMessages,
      printMessages : printMessages,
      hasInitialized : hasInitialized,
      getMessagesArray : getMessagesArray
  }
})();

var UIElements = (function(){
  var _maxDateMessages;
  var _amountMessages;

  var checkIfInputIsValid = function(){
    console.log("note: get this input thingy to work properly.");
    var date = document.getElementById('datepicker').value;
    var maxDate = new Date(date);
    var amount = parseInt(document.getElementById('amount').value);
    if(isNaN(maxDate.getTime())){
      alert("Please input a valid date.");
      return false;
    } else {
      maxDateMessages = maxDate;
    }
    if(!amount){
      alert("Please input a valid number as amount.");
      return false;
    } else {
      amountMessages = amount;
    }
    return true;
  }

  var generateGuildList = function(guildListArray){
    var guildDropDown = document.getElementById("guildnames");
    guildDropDown.innerHTML = "";
    var guildListString = "";
    for (var i = 0; i < guildListArray.length; i++){
      guildListString += '<button class="selectbtn" value="' + guildListArray[i].id + '">'+ guildListArray[i].name + '</button>';
    }
    guildDropDown.innerHTML = guildListString;
    _addClickEventListeners(guildDropDown.children, function(guildID){
      MessageData.setSelectedGuildID(guildID);
      toggleDropDown("guild");
    });
  }

  var generateChannelList = function(channelListArray){
    var channelDropDown = document.getElementById("channelnames");
    channelDropDown.innerHTML = "";
    var channelListString = "";
    for (var i = 0; i < channelListArray.length; i++){
      if(channelListArray[i].type == "text")
        channelListString += '<button class="selectbtn" value="' + channelListArray[i].id + '">'+ channelListArray[i].name + '</button>';
    }
    channelDropDown.innerHTML = channelListString;
    _addClickEventListeners(channelDropDown.children, function(channelID){
      MessageData.setSelectedChannelID(channelID);
      toggleDropDown("channel");
    });
  }

  var _addClickEventListeners = function(element, callback){
    for(var i = 0; i < element.length; i++) {
        element[i].onclick = function () {
            callback(this.value);
        };
    }
  }

  var toggleDropDown = function(input) {
    switch(input){
      case "channel":
      document.getElementById("ChannelDropdown").classList.toggle("show");
      break;
      case "guild":
      document.getElementById("GuildDropdown").classList.toggle("show");
      if(!MessageData.hasInitialized())
        MessageData.init();
      break;
    }
  }

  var enableGraphButton = function(){
    var btn = document.getElementById('graphButton');
    btn.disabled = false;
  }

  var getMessages = function(){
    MessageData.getMessages();
  }

  var getMaxDate = function(){
    return _maxDateMessages;
  }

  var getAmountMessages = function(){
    return _amountMessages;
  }

  return  {
    generateList :  generateGuildList,
    generateChannelList : generateChannelList,
    toggleDropDown : toggleDropDown,
    getMessages : getMessages,
    enableGraphButton : enableGraphButton,
    maxDate : getMaxDate,
    amount : getAmountMessages,
    checkIfInputIsValid : checkIfInputIsValid
  }
})();

var BarChart = (function(){
  var _endDate;
  var _startDate;
  var _messagesArray;
  var _amountDays;
  var _map = new Map();
  var _datasets = [];
  var _labels = [];
  var _data = {};

  //sets enddate to midnight of current day.
  var _setDates = function(){
    var date = new Date();
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);
    date.setMilliseconds(0);
    _endDate = date;
    _startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - 7);
    _amountDays = 7 - _endDate.getDay();
  }

  var _trimArray = function(array){
    var tempArr = [];
    console.log(`before array length: ${array.length}`);
    for (var i = 0; i < array.length; i++){
      var messageTime = new Date(array[i].createdTimestamp);
      if(messageTime < _endDate && messageTime > _startDate){
        array[i].day = messageTime.getDay();
        tempArr.push(array[i]);
      } else {
      }
    }
    console.log(`after array length: ${tempArr.length}`);
    _messagesArray = tempArr;
  }

  var _processMessages = function(){
    for (var i = 0; i < _messagesArray.length; i++){

      if(_map.has(_messagesArray[i].author)){
        var messagesOnDayArray = _map.get(_messagesArray[i].author);
        messagesOnDayArray[_messagesArray[i].day]++;
        _map.set(_messagesArray[i].author, messagesOnDayArray);
      } else {
        var messagesOnDayArray = [0,0,0,0,0,0,0];
        messagesOnDayArray[_messagesArray[i].day]++;
        _map.set(_messagesArray[i].author, messagesOnDayArray);
      }
      if(i + 1 == _messagesArray.length){
      }
    }
  }

  var _populateDataSetsArray = function(){
    for(var[k,v] of _map){
      var tempArr = [];
      for(var i = 0; i < v.length; i++){
        tempArr[(i + _amountDays) % 7] = v[i];
      }
      var obj = {
        label : k,
        data : tempArr,
        backgroundColor: _random_color()
      }
      _datasets.push(obj);
    }
  }

  var _random_color = function()
  {
    var rint = Math.round(0xffffff * Math.random());
    return 'rgba(' + (rint >> 16) + ',' + (rint >> 8 & 255) + ',' + (rint & 255) + ',0.4)';
  }

  var _generateLabels = function(){
    var standardlabels = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
    var labels = [];
    for(var i = 0; i < standardlabels.length; i++){
      labels[(i + _amountDays) % 7] = standardlabels[i];
    }
    _labels = labels;
  }

  var _createDataObject = function(){
    _data.labels = _labels;
    _data.datasets = _datasets;
  }

  var _renderChart = function (data){
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'bar',
      data: data
    });
  }

  var init = function(messagesArray) {
    _setDates();
    _trimArray(MessageData.getMessagesArray());
    _processMessages();
    _populateDataSetsArray();
    _generateLabels();
    _createDataObject();
    _renderChart(_data);
  }

  return {
    init : init
  }
})();

document.addEventListener("DOMContentLoaded", function(){
    MessageData.init();
});

//should rewrite this code if it ever gets used in other places.
function filterFunction(input) {
    var input, filter, ul, li, a, i;
    if(input == "GuildInput"){
      input = document.getElementById("GuildInput");
      div = document.getElementById("GuildDropdown");
    } else if (input == "ChannelInput"){
      input = document.getElementById("ChannelInput");
      div = document.getElementById("ChannelDropdown");
    }
    filter = input.value.toUpperCase();
    a = div.getElementsByTagName("button");
    for (i = 0; i < a.length; i++) {
        if (a[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
            a[i].style.display = "";
        } else {
            a[i].style.display = "none";
        }
    }
}
