var BotConfig = (function(){
  //private variables.
  var _values;
  var _getValues = function(){
    socket.emit('getvalues', "getting bot values");
  }
  var _setValue = function(valueName, value){
    socket.emit('setvalue', valueName, value);
  }

  var _generateRadios = function(){
    var configMenu = document.settings;
    var table = '';
    table += '<table class="settingstable">';
    table += '<th>Name:</th><th>ON</th><th>OFF</th>';
    for (value in _values){
      if(_values[value]){
        table += '<tr><td> ' + value + '</td><td> <input type ="radio" name="' + value + '" value="true" checked="checked" /></td>'
        table += ' <td><input type ="radio" name="' + value + '" value="false" /></td></tr>'
      } else {
        table += '<tr><td> ' + value + ' </td><td><input type ="radio" name="' + value + '" value="true" /></td>'
        table += ' <td><input type ="radio" name="' + value + '" value="false" checked="checked" /></td></tr>'
      }
    }
    table += "</table>";
    configMenu.innerHTML = table;
    _addClickEventListeners(configMenu);
  }

  var _addClickEventListeners = function(element){
    rad = element;
    var prev = null;
    for(var i = 0; i < rad.length; i++) {
      if(rad[i].type == "radio"){
        rad[i].onclick = function () {
            _setValue(this.name, this.value);
        };
      }
    }
  }

  //listeners for socket calls from server.
  socket.on('botvalues', function(data){
    _values = data;
    for (value in BotConfig.getValueList()){
    }
    _generateRadios();
  });

  //public methods.
  var getValueList = function(){
    return _values;
  }

  var updateValues = function(values){
    _values = values;
  }

  var setValue = function(valueName, value){
    _setValue(valueName, value);
  }

  var generateList = function(){
    _getValues();
  }

  //return statement using revealing module pattern.
  return {
    updateValues : updateValues,
    setValue : setValue,
    generateList : generateList,
    getValueList : getValueList
  }
})();

document.addEventListener("DOMContentLoaded", function(){
    BotConfig.generateList();
});
