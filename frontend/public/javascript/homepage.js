var homepage = (function(){
  var _element;

  var _getGeneralData = function(){
    socket.emit('getgeneraldata');
  }

  socket.on('generaldata', function(name, uptime, guilds, channels, users, premium){
    _element.innerHTML = "Logged in as user: " + name + "<br>Server uptime: " + uptime + " seconds.<br>";
    _element.innerHTML += "Member of " + guilds + " servers, " + channels + " channels and sharing a server with " + users + " users.<br>";
    _element.innerHTML += "Premium active: ";
    _element.innerHTML += premium ? "Yes" : "No";
  });

  var init = function(){
    _element = document.getElementById('info');
    _getGeneralData();
  }
  
  return {
    init : init
  }
})();

document.addEventListener("DOMContentLoaded", function(){
    homepage.init();
});
